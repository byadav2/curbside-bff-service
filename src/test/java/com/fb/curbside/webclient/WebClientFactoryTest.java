package com.fb.curbside.webclient;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("ConstantConditions")
@ExtendWith(MockitoExtension.class)
public class WebClientFactoryTest {

    @Mock
    private Connection connection;

    private WebClientFactory webClientFactory;

    @BeforeEach
    public void init() {
        var config = new WebClientConfig();
        config.setConnectionTimeOut(100);
        config.setReadTimeOut(10);
        config.setWriteTimeOut(1);
        webClientFactory = new WebClientFactory(config, WebClient.builder());
    }

    @Test
    public void shouldGetClientResponseMonoFunction() {
        when(connection.addHandlerLast(ArgumentMatchers.any(ReadTimeoutHandler.class))).thenReturn(connection);
        when(connection.addHandlerLast(ArgumentMatchers.any(WriteTimeoutHandler.class))).thenReturn(connection);
        Consumer<Connection> consumer = webClientFactory.createConnectionConsumer();
        consumer.accept(connection);
        assertNotNull(consumer);
        verify(connection, times(1)).addHandlerLast(ArgumentMatchers.any(ReadTimeoutHandler.class));
        verify(connection, times(1)).addHandlerLast(ArgumentMatchers.any(ReadTimeoutHandler.class));
    }

    @Test
    public void logRequest() throws URISyntaxException {
        ClientRequest originalClientRequest = ClientRequest.create(HttpMethod.GET, new URI("http://localhost")).build();

        Mono<ClientRequest> clientRequest = ReflectionTestUtils.invokeMethod(webClientFactory, "logRequest", originalClientRequest);

        assertNotNull(clientRequest.block());
    }

    @Test
    public void logResponse() {
        ClientResponse originalClientResponse = ClientResponse.create(HttpStatus.OK).build();

        Mono<ClientResponse> clientResponse = ReflectionTestUtils.invokeMethod(webClientFactory, "logResponse", originalClientResponse);

        assertNotNull(clientResponse.block());
    }
}