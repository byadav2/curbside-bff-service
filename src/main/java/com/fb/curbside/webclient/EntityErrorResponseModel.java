package com.fb.curbside.webclient;

import java.util.Arrays;

import lombok.Data;

@Data
public class EntityErrorResponseModel {
    private Arrays errors;

    public Arrays getErrors() {
        return errors;
    }

    public void setErrors(Arrays errors) {
        this.errors = errors;
    }
}
