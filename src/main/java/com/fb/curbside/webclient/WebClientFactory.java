package com.fb.curbside.webclient;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Slf4j
@Component
public class WebClientFactory {

    private final WebClientConfig webClientConfig;
    private final WebClient.Builder builder;
    private static final int MEGA_BYTE = 1024 * 1024;

    public WebClientFactory(WebClientConfig webClientConfig, WebClient.Builder builder) {
        this.webClientConfig = webClientConfig;
        this.builder = builder;
    }

    public WebClient newClient(String baseUrl) {
        return builder.clone()
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configure -> configure
                                .defaultCodecs()
                                .maxInMemorySize(webClientConfig.getMaxInMemory() * MEGA_BYTE))
                        .build())
                .baseUrl(baseUrl)
                .filters(fns -> {
                    fns.add(ExchangeFilterFunction.ofRequestProcessor(this::logRequest));
                    fns.add(ExchangeFilterFunction.ofResponseProcessor(this::logResponse));
                })
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(createTcpClient())))
                .build();
    }

    protected TcpClient createTcpClient() {
        return TcpClient
                .create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, webClientConfig.getConnectionTimeOut())
                .doOnConnected(createConnectionConsumer());
    }

    protected Consumer<Connection> createConnectionConsumer() {
        return connection -> {
            connection.addHandlerLast(new ReadTimeoutHandler(webClientConfig.getReadTimeOut(), TimeUnit.MILLISECONDS));
            connection.addHandlerLast(new WriteTimeoutHandler(webClientConfig.getWriteTimeOut(), TimeUnit.MILLISECONDS));
        };
    }

    /***
     * A helper function that logs ClientRequests being initiated by the BFF
     * @param clientRequest The ClientRequest that should be logged
     * @return The original ClientRequest is returned unmodified.
     */
    private Mono<ClientRequest> logRequest(final ClientRequest clientRequest) {
        log.debug("--> {} {} {}", clientRequest.logPrefix(), clientRequest.method(), clientRequest.url());
        return Mono.just(clientRequest);
    }

    /***
     * A helper function that logs ClientResponses received to requests initiated by the BFF
     * @param clientResponse The ClientResponse that should be logged
     * @return The original ClientResponse is returned unmodified.
     */
    private Mono<ClientResponse> logResponse(final ClientResponse clientResponse) {
        log.debug("<-- {} {} - {}", clientResponse.logPrefix(), clientResponse.statusCode(), clientResponse.statusCode().getReasonPhrase());
        return Mono.just(clientResponse);
    }
}
