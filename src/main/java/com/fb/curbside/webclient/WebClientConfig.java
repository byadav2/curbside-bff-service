package com.fb.curbside.webclient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@NoArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "webclient")
@Getter
@Setter
public class WebClientConfig {

    private int connectionTimeOut;
    private int readTimeOut;
    private int writeTimeOut;
    private int maxInMemory;

}
