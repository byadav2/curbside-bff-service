package com.fb.curbside.webclient;

import com.fb.curbside.exceptions.ApplicationErrorException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WebClientResponseErrorHandler {

    private static final String ERROR_STRING = "op=handleError status={} , Response={}";

    public static Mono<Throwable> handleError(ClientResponse clientResponse) {
        HttpStatus status = clientResponse.statusCode();
        log.error(ERROR_STRING, status, clientResponse);
        throw new ApplicationErrorException(status.getReasonPhrase());
    }

}
