package com.fb.curbside.store.service;

import java.util.concurrent.CompletableFuture;

import com.fb.curbside.store.domain.StoreResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Stores service
 */
@Slf4j
@Service
public class StoresService {

    private final WebClient webClient;

    public StoresService(@Qualifier("storeServiceWebClient") WebClient webClient) {
        this.webClient = webClient;
    }

    public CompletableFuture<StoreResponse> createStore(String store) {
        return null;

    }

    public CompletableFuture<StoreResponse> getStore(String storeId) {
        StoreResponse storeResponse = new StoreResponse();
        storeResponse.setStoreId("1234");
        return CompletableFuture.completedFuture(storeResponse);
    }

    public CompletableFuture<StoreResponse> deleteStore(String storeId) {
        return null;
    }

    public CompletableFuture<StoreResponse> updateStore(String storeId, String store) {

        return null;
    }
}
