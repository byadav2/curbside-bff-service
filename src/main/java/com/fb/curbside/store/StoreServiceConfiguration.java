package com.fb.curbside.store;

import com.fb.curbside.webclient.WebClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@EnableConfigurationProperties(StoreServiceProperties.class)
@Configuration
public class StoreServiceConfiguration {

    private final StoreServiceProperties properties;
    private final WebClientFactory webClientFactory;

    @Autowired
    public StoreServiceConfiguration(StoreServiceProperties properties,
                                     WebClientFactory webClientFactory) {
        this.properties = properties;
        this.webClientFactory = webClientFactory;
    }

    @Bean("storeServiceWebClient")
    public WebClient storeServiceWebClient() {
        return webClientFactory.newClient(properties.getBaseUrl());
    }
}
