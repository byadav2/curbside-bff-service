package com.fb.curbside.store.controller;

import java.util.concurrent.CompletableFuture;

import com.fb.curbside.store.domain.StoreResponse;
import com.fb.curbside.store.service.StoresService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/web-bff/accounts/{accountId}")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class StoresController {

    private final StoresService storesService;

    public StoresController(StoresService storesService) {
        this.storesService = storesService;
    }

    @PostMapping("/stores")
    public CompletableFuture<StoreResponse> createStore(@PathVariable String accountId,
                                                        @RequestBody String store) {
        return storesService.createStore(store);
    }

    @GetMapping("/stores/{storeId}")
    public CompletableFuture<StoreResponse> getStore(@PathVariable String accountId,
                                                     @PathVariable String storeId) {
        return storesService.getStore(storeId);
    }

    @DeleteMapping("/stores/{storeId}")
    public CompletableFuture<StoreResponse> deleteStore(@PathVariable String accountId,
                                                     @PathVariable String storeId) {
        return storesService.deleteStore(storeId);
    }

    @PutMapping("/stores/{storeId}")
    public CompletableFuture<StoreResponse> updateStore(@PathVariable String accountId,
                                                        @PathVariable String storeId,
                                                        @RequestBody String store) {
        return storesService.updateStore(storeId, store);
    }
}
