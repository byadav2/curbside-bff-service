package com.fb.curbside.store;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("store-service")
public class StoreServiceProperties {
    private String baseUrl;
}
