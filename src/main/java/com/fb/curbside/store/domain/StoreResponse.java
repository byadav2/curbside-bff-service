package com.fb.curbside.store.domain;

import lombok.Data;

@Data
public class StoreResponse {
    private String storeId;
}
