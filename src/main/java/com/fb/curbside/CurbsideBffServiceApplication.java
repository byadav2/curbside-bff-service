package com.fb.curbside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurbsideBffServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurbsideBffServiceApplication.class, args);
	}

}
