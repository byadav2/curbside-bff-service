package com.fb.curbside.auth.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/web-bff")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class AuthController {

    @PostMapping("/accounts/login")
    public ResponseEntity<?> getStatus(@RequestBody String input) {
        // mocked.
        return ResponseEntity.ok("OK");
    }

}
