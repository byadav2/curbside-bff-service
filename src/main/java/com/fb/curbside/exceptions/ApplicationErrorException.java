package com.fb.curbside.exceptions;

/**
 * Exception thrown for Application error.
 */
public class ApplicationErrorException extends RuntimeException {

    /**
     * Constructor for the Application Error.
     *
     * @param message for the error.
     */
    public ApplicationErrorException(final String message) {
        super(message);
    }

}
