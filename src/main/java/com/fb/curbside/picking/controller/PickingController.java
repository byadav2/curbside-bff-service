package com.fb.curbside.picking.controller;

import java.util.List;

import com.fb.curbside.picking.domain.OrderSlotDetails;
import com.fb.curbside.picking.domain.SchedulePickupRequest;
import com.fb.curbside.picking.domain.SlotDetailsResponseDTO;
import com.fb.curbside.picking.service.PickingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/web-bff/accounts/{accountId}")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class PickingController {

    private final PickingService pickingService;

    public PickingController(PickingService pickingService) {
        this.pickingService = pickingService;
    }

    @PostMapping("/picking/initiate")
    public Mono<SlotDetailsResponseDTO> initiatePicking(@PathVariable String accountId,
                                                        @RequestParam String lat,
                                                        @RequestParam String lng,
                                                        @RequestParam String storeId,
                                                        @RequestParam(required = false) String slotDate) {
        return pickingService.initiatePicking(storeId, lat, lng, slotDate);
    }

    @PostMapping("/pickup/schedule")
    public Boolean pickupSchedule(@PathVariable String accountId,
                                  @RequestBody SchedulePickupRequest schedulePickupRequest) {
        return pickingService.pickupSchedule(schedulePickupRequest);
    }

    @GetMapping("/pickup/scheduled")
    public Mono<List<OrderSlotDetails>> getBookingDetails(@PathVariable String accountId) {
        return pickingService.getBookingDetails(accountId);
    }
}
