package com.fb.curbside.picking;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("picking-service")
public class PickingServiceProperties {
    private String baseUrl;
}
