package com.fb.curbside.picking;

import com.fb.curbside.webclient.WebClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@EnableConfigurationProperties(PickingServiceProperties.class)
@Configuration
public class PickingServiceConfiguration {

    private final PickingServiceProperties properties;
    private final WebClientFactory webClientFactory;

    @Autowired
    public PickingServiceConfiguration(PickingServiceProperties properties,
                                       WebClientFactory webClientFactory) {
        this.properties = properties;
        this.webClientFactory = webClientFactory;
    }

    @Bean("pickingServiceWebClient")
    public WebClient pickingServiceWebClient() {
        return webClientFactory.newClient(properties.getBaseUrl());
    }
}
