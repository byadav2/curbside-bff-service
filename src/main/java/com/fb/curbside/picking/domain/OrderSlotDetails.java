package com.fb.curbside.picking.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderSlotDetails {
    String id;
    BookingDetails bookingDetails;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class BookingDetails {
        String storeId;
        LocalDate slotDate;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
        LocalDateTime slotStartTime;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
        LocalDateTime slotEndTime;
        String ruth;
        String name;
        Double latitude;
        Double longitude;
    }
}
