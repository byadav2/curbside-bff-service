package com.fb.curbside.picking.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class SlotDetailsResponseDTO {

    List<SlotDetailsResponseWrapper> storeDetails;

    @Data
    public static class SlotDetailsResponseWrapper {
        String storeId;
        List<StoreWiseSlotDetails> allDaysSlotDetailList;
    }

    @Data
    public static class StoreWiseSlotDetails {
        double distance;
        LocalDate slotDate;
        Collection<SlotDetailsDTO> slotDetailsDTOList;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class SlotDetailsDTO {
            @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
            LocalDateTime slotStartTime;
            @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
            LocalDateTime slotEndTime;
        }
    }

}
