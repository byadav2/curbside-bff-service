package com.fb.curbside.picking.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class SchedulePickupRequest {
    String storeId;
    LocalDate slotDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotStartTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime slotEndTime;
    String customerId;
    String orderId;
    String ruth;
    String name;
}
