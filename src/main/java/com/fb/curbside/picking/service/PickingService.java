package com.fb.curbside.picking.service;

import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fb.curbside.outbound.domain.Order;
import com.fb.curbside.outbound.service.OutboundService;
import com.fb.curbside.picking.domain.OrderSlotDetails;
import com.fb.curbside.picking.domain.SchedulePickupRequest;
import com.fb.curbside.picking.domain.SlotDetailsResponseDTO;
import com.fb.curbside.webclient.WebClientResponseErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

/**
 * Picking service
 */
@Slf4j
@Service
public class PickingService {

    private final WebClient webClient;
    private final OutboundService outboundService;

    public PickingService(@Qualifier("pickingServiceWebClient") WebClient webClient,
                          OutboundService outboundService) {
        this.webClient = webClient;
        this.outboundService = outboundService;
    }

    public Mono<SlotDetailsResponseDTO> initiatePicking(String storeId, String lat, String lng, String slotDate) {
        return webClient.get()
                .uri(builder -> buildFindSlotsUrl(storeId, lat, lng, slotDate, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(SlotDetailsResponseDTO.class);
    }


    /**
     * Builds the load URL by through a {@link UriBuilder}.
     *
     * @param storeId String
     * @param builder UriBuilder
     * @return Returns a {@link URL}.
     */
    private URI buildFindSlotsUrl(String storeId, String lat, String lng, String slotDate, UriBuilder builder) {
        builder.path(FIND_SLOTS);
        if (!StringUtils.isEmpty(storeId)) {
            builder.queryParam("store_id", storeId);
        }
        if (!StringUtils.isEmpty(storeId)) {
            builder.queryParam("lat", lat);
        }
        if (!StringUtils.isEmpty(storeId)) {
            builder.queryParam("lng", lng);
        }
        if (!StringUtils.isEmpty(storeId)) {
            builder.queryParam("slot_date", slotDate);
        }
        return builder.build();
    }

    public Boolean pickupSchedule(SchedulePickupRequest schedulePickupRequest) {
        Boolean aBoolean = webClient.post()
                .uri(this::buildPickupScheduleUrl)
                .body(BodyInserters.fromValue(schedulePickupRequest))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Boolean.TYPE)
                .defaultIfEmpty(Boolean.TRUE).block();

        if (Boolean.TRUE.equals(aBoolean)) {
            log.info("op=updateOrder, status=OK, desc=Update order status");
            String orderId = schedulePickupRequest.getOrderId();
            Mono<Order> orderMono = outboundService.getOrder(orderId);
            Order order = orderMono.block();
            assert order != null;
            order.setStatus("SCHEDULED");
            Mono<Order> updatedOrderMono = outboundService.updateOrder(orderId, order);
            Order updatedOrder = updatedOrderMono.block();
            assert updatedOrder != null;
            log.info("op=updatedOrder, status=OK, desc=Update order status to {}", updatedOrder.getStatus());
        }
        return aBoolean;
    }

    private URI buildPickupScheduleUrl(UriBuilder builder) {
        builder.path(SCHEDULE);
        return builder.build();
    }

    public Mono<List<OrderSlotDetails>> getBookingDetails(String accountId) {
        Set<Long> orderIds = outboundService.getAllScheduledOrders(accountId);
        if (CollectionUtils.isEmpty(orderIds)) {
            return Mono.empty();
        }
        return webClient.get()
                .uri(builder -> buildBookingDetailsUrl(orderIds, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    private URI buildBookingDetailsUrl(Set<Long> orderIds, UriBuilder builder) {
        builder.path(ORDERS);
        if (!CollectionUtils.isEmpty(orderIds)) {
            builder.queryParam("orderIds", orderIds);
        }
        return builder.build();
    }

    private static final String SCHEDULE = "/v1/slots";
    private static final String FIND_SLOTS = "/v1/slots";
    private static final String ORDERS = "/v1/orders";

}
