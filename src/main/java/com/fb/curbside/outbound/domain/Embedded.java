package com.fb.curbside.outbound.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
class Self {
    private String href;
}

@Data
class Self_ {
    private String href;
}

@Data
class Links_ {
    private Self_ self;
}

@Data
class Links {
    private Self self;
    private Order_ order;
}

@Data
class Order_ {
    private String href;
}

@Data
public class Embedded {
    private List<Order> orders = new ArrayList<Order>();
}