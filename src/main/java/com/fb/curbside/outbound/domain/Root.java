package com.fb.curbside.outbound.domain;

import lombok.Data;

@Data
public class Root {
    private Embedded _embedded;
    private Links_ links;
}
