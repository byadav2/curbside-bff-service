package com.fb.curbside.outbound.domain;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Customer extends BaseEntity {
    private String firstname, lastname;
    private Set<Address> addresses = new HashSet<>();

}
