package com.fb.curbside.outbound.domain;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Order extends BaseEntity {

    private Set<LineItem> lineItems = new HashSet<>();
    private Customer customer;
    private Address billingAddress;
    private Address shippingAddress;
    private String storeId;
    private String status;
    private String accountId;
}
