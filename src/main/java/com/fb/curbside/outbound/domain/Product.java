package com.fb.curbside.outbound.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Product extends BaseEntity {

    private String name;
    private String description;
    private BigDecimal price;

    private Map<String, String> attributes = new HashMap<>();
}
