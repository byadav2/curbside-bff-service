package com.fb.curbside.outbound.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LineItem extends BaseEntity {

    private Product product;
    private BigDecimal price;
    private int amount;

}
