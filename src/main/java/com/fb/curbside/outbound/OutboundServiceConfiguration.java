package com.fb.curbside.outbound;

import com.fb.curbside.webclient.WebClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@EnableConfigurationProperties(OutboundServiceProperties.class)
@Configuration
public class OutboundServiceConfiguration {

    private final OutboundServiceProperties properties;
    private final WebClientFactory webClientFactory;

    @Autowired
    public OutboundServiceConfiguration(OutboundServiceProperties properties,
            WebClientFactory webClientFactory) {
        this.properties = properties;
        this.webClientFactory = webClientFactory;
    }

    @Bean("outboundServiceWebClient")
    public WebClient outboundServiceWebClient() {
        return webClientFactory.newClient(properties.getBaseUrl());
    }
}
