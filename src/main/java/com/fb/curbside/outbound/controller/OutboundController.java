package com.fb.curbside.outbound.controller;

import com.fb.curbside.outbound.service.OutboundService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/web-bff/accounts/{accountId}")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class OutboundController {

    private final OutboundService outboundService;

    public OutboundController(OutboundService outboundService) {
        this.outboundService = outboundService;
    }

    @GetMapping("/store/orders")
    public Mono<Object> getAllStorePickUpOrders(@PathVariable String accountId) {
        return outboundService.getAllStorePickUpOrders(accountId);
    }

    @GetMapping("/orders")
    public Mono<Object> getAllOrders(@PathVariable String accountId) {
        return outboundService.getAllOrders(accountId);
    }

}
