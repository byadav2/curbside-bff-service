package com.fb.curbside.outbound;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("outbound-service")
public class OutboundServiceProperties {
    private String baseUrl;
}
