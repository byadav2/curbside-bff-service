package com.fb.curbside.outbound.service;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.fb.curbside.outbound.domain.BaseEntity;
import com.fb.curbside.outbound.domain.Embedded;
import com.fb.curbside.outbound.domain.Order;
import com.fb.curbside.outbound.domain.Root;
import com.fb.curbside.picking.domain.OrderSlotDetails;
import com.fb.curbside.picking.service.PickingService;
import com.fb.curbside.webclient.WebClientResponseErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

/**
 * Outbound service
 */
@Slf4j
@Service
public class OutboundService {

    public static final String PLACED = "PLACED";
    private final WebClient webClient;

    public OutboundService(@Qualifier("outboundServiceWebClient") WebClient webClient) {
        this.webClient = webClient;
    }

    /**
     * Return all the order with status PLACED and are store orders
     * @param accountId String
     */
    public Mono<Object> getAllStorePickUpOrders(String accountId) {
        return webClient.get()
                .uri(builder -> buildFindByAccountId(accountId, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Root.class)
                .map(Root::get_embedded)
                .map(Embedded::getOrders)
                .map(orders -> orders.stream()
                        .filter(getOrderPredicate())
                        .collect(Collectors.toSet()));
    }

    private Predicate<Order> getOrderPredicate() {
        return order -> !StringUtils.isEmpty(order.getStoreId())
                && PLACED.equalsIgnoreCase(order.getStatus());
    }

    /**
     * Builds the load URL by through a {@link UriBuilder}.
     * @param accountId String
     * @param builder UriBuilder
     * @return Returns a {@link URL}.
     */
    private URI buildFindByAccountId(String accountId, UriBuilder builder) {
        builder.path(FIND_BY_ACCOUNT_ID);
        if (!StringUtils.isEmpty(accountId)) {
            builder.queryParam("accountId", accountId);
        }
        return builder.build();
    }

    /**
     * Return all the store order with status PLACED/DELIVERED/SCHEDULED
     * @param accountId String
     */
    public Mono<Object> getAllOrders(String accountId) {
        return webClient.get()
                .uri(builder -> buildFindByAccountId(accountId, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Root.class)
                .map(Root::get_embedded)
                .map(Embedded::getOrders)
                .map(orders -> orders.stream()
                        .filter(order -> !StringUtils.isEmpty(order.getStoreId()))
                        .collect(Collectors.toSet()));
    }

    public Mono<Order> getOrder(String id) {
        return webClient.get()
                .uri(builder -> buildGetById(id, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Order.class);
    }

    private URI buildGetById(String id, UriBuilder builder) {
        builder.path(GET_ORDER);
        if (!StringUtils.isEmpty(id)) {
            builder.path(id);
        }
        return builder.build();
    }

    public Mono<Order> updateOrder(String id, Order order) {
        return webClient.put()
                .uri(builder -> buildUpdateById(id, builder))
                .body(BodyInserters.fromValue(order))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Order.class);
    }

    private URI buildUpdateById(String id, UriBuilder builder) {
        builder.path(UPDATE_ORDER);
        if (!StringUtils.isEmpty(id)) {
            builder.path(id);
        }
        return builder.build();
    }

    public Set<Long> getAllScheduledOrders(String accountId) {
        List<Order> ordersList = webClient.get()
                .uri(builder -> buildFindByAccountId(accountId, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(Root.class)
                .map(Root::get_embedded)
                .map(Embedded::getOrders)
                .block();
        assert ordersList != null;
        List<Order> scheduledOrderList = getScheduledOrderList(ordersList);
        return scheduledOrderList.stream()
                .map(BaseEntity::getId).collect(Collectors.toSet());
    }

    private List<Order> getScheduledOrderList(List<Order> ordersList) {
        return ordersList.stream()
                .filter(order -> SCHEDULED.equalsIgnoreCase(order.getStatus()))
                .collect(Collectors.toList());
    }

    private static final String FIND_BY_ACCOUNT_ID = "/orders/search/findByAccountId";
    private static final String GET_ORDER = "/orders/";
    private static final String UPDATE_ORDER = "/orders/";
    public static final String SCHEDULED = "SCHEDULED";
}
