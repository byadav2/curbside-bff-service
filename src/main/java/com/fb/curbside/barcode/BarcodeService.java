package com.fb.curbside.barcode;

import java.net.URI;

import com.fb.curbside.webclient.WebClientResponseErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class BarcodeService {
    private final WebClient webClient;

    public BarcodeService(@Qualifier("barcodeServiceWebClient") WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<String> barcodeGenerate(String accountId, String orderId) {
        return webClient.get()
                .uri(builder -> buildBarcodeGeneratedUrl(accountId, orderId, builder))
                .retrieve()
                .onStatus(HttpStatus::isError, WebClientResponseErrorHandler::handleError)
                .bodyToMono(String.class);
    }

    private URI buildBarcodeGeneratedUrl(String accountId, String orderId, UriBuilder builder) {
        builder.path(SCHEDULE);
        if (!StringUtils.isEmpty(accountId) && !StringUtils.isEmpty(orderId)) {
            builder.path(accountId + ":" + orderId);
        }
        return builder.build();
    }

    private static final String SCHEDULE = "/generate_code/";
}
