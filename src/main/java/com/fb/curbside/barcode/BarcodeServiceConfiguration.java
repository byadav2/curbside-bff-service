package com.fb.curbside.barcode;

import com.fb.curbside.webclient.WebClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@EnableConfigurationProperties(BarcodeServiceProperties.class)
@Configuration
public class BarcodeServiceConfiguration {

    private final BarcodeServiceProperties properties;
    private final WebClientFactory webClientFactory;

    @Autowired
    public BarcodeServiceConfiguration(BarcodeServiceProperties properties,
                                       WebClientFactory webClientFactory) {
        this.properties = properties;
        this.webClientFactory = webClientFactory;
    }

    @Bean("barcodeServiceWebClient")
    public WebClient barcodeServiceWebClient() {
        return webClientFactory.newClient(properties.getBaseUrl());
    }
}
