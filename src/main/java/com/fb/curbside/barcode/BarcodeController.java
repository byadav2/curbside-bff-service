package com.fb.curbside.barcode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/web-bff/accounts/{accountId}")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class BarcodeController {

    private final BarcodeService barcodeService;

    public BarcodeController(BarcodeService barcodeService) {
        this.barcodeService = barcodeService;
    }

    @GetMapping("/barcode")
    public Mono<String> barcodeGenerate(@PathVariable String accountId,
                                        @RequestParam String orderId) {
        return barcodeService.barcodeGenerate(accountId, orderId);
    }

}
